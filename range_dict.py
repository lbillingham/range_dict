# -*- coding: utf-8 -*-
"""
One class file, see the class docstring
Created on Fri Jun 05 18:58:34 2015

@author: laurence
"""
from __future__ import print_function
import doctest

KEY_ERR = ('RangeDict key must be a length-two '
           'collection supporting binary comparison')


def _check_key_len(key):
    """
    Check we have length 2 collections as keys.
    We need this for bounds specification.

    Parameters
    ----------
    keys : a collection
        The candidate key whose length is to be checked

    Raises
    ------
    ValueError:
        if the key is not the right length
    """
    if len(key) != 2:
        raise ValueError(KEY_ERR)


def _check_bounds(key):
    """
    Check that the `key` elements are in order
    `k[0]` is a lower bound, `k[1]` is an upper bound.

    Parameters
    ----------
    keys : a collection
        The candidate key to be checked

    Raises
    ------
    RuntimeError:
        if the key bounds do not make sense
    """
    if key[0] > key[1]:
        raise RuntimeError('RangeDict key must '
                           'have key[0] < key[1]')


class RangeDict(dict):
    """
    Like a dictionary mapping but the keys are upper and lower bounds.
    Return value based on key falling within a range.
    Lower endpoint included.
    """
    def __init__(self, d={}):
        """
        Can be instatiated empty (default) or initialized with curly braces
        like a `dict` see 'Parameters' below.

        Parameters
        ----------
        d : dictionary like curly braces,
            e.g. `RangeDict({(0, 3): 'ikkle', (4, 1e6): 'ginormous'})`

        Examples
        --------
        >>> rd = RangeDict({(0, 3): 'ikkle', (4, 1e6): 'ginormous'})
        >>> rd[2]
        'ikkle'

        >>> rd.get(100)
        'ginormous'
        """
        for k, v in d.items():
            self[k] = v

    def __repr__(self):
        """
        Gather the ranges and  output values then print as for `dict`

        Returns
        -------
        string
            message representing the RangeDict instance
        """
        elems = ['range{0}: {1}'.format(k, v) for k, v in self.items()]
        mess = ', '.join(elems)
        return '{' + mess + '}\n'

    def _check_overlaps(self, new_key):
        """
        Check a new key being added does not overlap with any keys previously
        inserted.

         Parameters
        ----------
        new_key : len 2 collection supporting binary comparison
           Candidate key to be checked

        Raises
        ------
        RuntimeError:
            if the new key overlaps any of the existing key ranges
        """
        highside_olap = [new_key[0] < k[1] and new_key[0] > k[0]
                         for k in self.keys()]
        lowside_olap = [new_key[1] < k[1] and new_key[1] > k[0]
                        for k in self.keys()]
        if any(highside_olap + lowside_olap):
            raise RuntimeError('new RangeDict key {0} must '.format(new_key) +
                               'not overlap '
                               'existing keys: {0}'.format(self.keys()))

    def __setitem__(self, key, value):
        """
        Add a `key`, `value` pair to the RangeDict instance.

        Parameters
        ----------
        key : len(2) collection supporting binary comparison
            key[0] defines the lower bound
            key[1] the upper bound
        value :
            the value to return when given a single sub-key
        """
        try:
            _check_key_len(key)
            _check_bounds(key)
            self._check_overlaps(key)
            dict.__setitem__(self, (key[0], key[1]), value)

        except TypeError:
            raise TypeError(KEY_ERR)

    def __contains__(self, key):
        """
        Does existing object already contain `key`

        Parameters
        ----------
        key :
            new candidate key to check

        Returns
        -------
        bool
            is `key` already in the RangeDict
        """
        try:
            return bool(self[key]) or True
        except KeyError:
            return False

    @property
    def upper_bounds(self):
        """
        List all the upper bounds in the RangeDict

        Returns
        -------
        List of the upper bounds (key[1]s) in the `RangeDict`
        """
        return [k[1] for k in self.keys()]

    @property
    def lower_bounds(self):
        """
        List all the lower bounds in the RangeDict

        Returns
        -------
        List of the lower bounds (key[0]s) in the `RangeDict`
        """
        return [k[0] for k in self.keys()]

    def __getitem__(self, key):
        """
        Return the `value` that is mapped to the range in which `key` lies

        Parameters
        ----------
        key :
            object that may lie between the bounds of the RangeDicts's keys

        Returns
        -------
        item if `key` lies within existing bounds

        Raises
        ------
        KeyError if `key` not within bounds of existing keys
        """
        for k, v in self.items():
            if k[0] <= key < k[1]:
                return v
        raise KeyError("Key'{0}' outwith values in the RangeDict" .format(key))

    def get(self, key, default=None):
        """
        like [] __getitem__ but raises no error, returns a `default` value

        Parameters
        ----------
        key :
            object that may lie between the bounds of the RangeDicts's keys

        default : optional
            Return this if `key` not within existing bounds. Defaults to `None`

        Returns
        -------
        item if `key` lies within existing bounds or `default` if it does not.

        Raises
        ------
        None

        """
        if self.__contains__(key):
            return self.__getitem__(key)
        else:
            return default

if '__main__' == __name__:
    doctest.testmod()
